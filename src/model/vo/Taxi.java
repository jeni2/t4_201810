package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{

	private String company;
	private String taxiId;
	private int nServicios;
	
	
	public Taxi(String c, String id /** int nServi*/)
	{
		company=c;
		taxiId=id;
		//setnServicios(nServi);
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiId;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		return 0;
	}
	public int getnServicios() {
		return nServicios;
	}
	public void setnServicios(int nServicios) {
		this.nServicios = nServicios;
	}	
}
