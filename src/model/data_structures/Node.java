package model.data_structures;

import java.util.Date;

import com.sun.javafx.scene.paint.GradientUtils.Point;

public class Node <E>{


	private Node<E> next;
	private Node<E> previous;
	
	private E item;

	public Node (E item) {
		next = null;
		previous=null;
		this.item = item;
	}

	public Node<E> getNext() {
		return next;
	}

	public void setNextNode ( Node<E> pNext) {
		next = pNext;
	}

	public E getItem(){
		return item;
	}

	public void setItem (E pItem) {
		item = pItem;
	}

	public boolean hasNext() {
		// TODO Auto-generated method stub
		boolean resp=false;
		if(next!=null)
		{
			resp= true;
		}
		return resp;
	}

	public Node<E> getPrevious() {
		return previous;
	}

	public void setPrevious(Node<E> previous) {
		this.previous = previous;
	}

}
