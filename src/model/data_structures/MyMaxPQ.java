package model.data_structures;

public class MyMaxPQ <Key extends Comparable<Key>> implements IMaxPQ<Key> {

	private Key[] pq; // pq[i] = ith element on pq
	private int N; // number of elements on pq


	@Override
	public void MaxPQ() {
		// TODO Auto-generated method stub

	}

	@Override
	public void MaxPQ(int max) {
		// TODO Auto-generated method stub
		pq = (Key[]) new Comparable [ max ]; }


	@Override
	public void MaxPQ(Key[] a) {
		// TODO Auto-generated method stub

	}

	@Override
	public void insert(Key v) {
		// TODO Auto-generated method stub
		{ pq[N++] = v; }
	}

	@Override
	public Key max() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Key delMax() {
		// TODO Auto-generated method stub
		int max = 0;
		for (int i = 1; i < N; i++)
			if (less(max, i)) max = i;
		exch(max, N-1);
		return pq[--N];
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		{ return N == 0; }
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return N;
	}
	private boolean less (int i, int j)
	{ 
		return pq[i].compareTo(pq[j]) < 0; 
	}
	private void exch (int i, int j)
	{ 
		Key t = pq[i]; 
		pq[i] = pq[j]; 
		pq[j] = t; 
	}
	private void swim (int k)
	{
		while (k > 1 && less(k/2, k))
		{
			exch(k, k/2);
			k = k/2;
		}
	}



}
